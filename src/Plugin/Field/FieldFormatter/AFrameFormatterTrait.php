<?php

namespace Drupal\aframe\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AFrameFormatterTrait.
 *
 * @package Drupal\aframe\Plugin\Field\FieldFormatter
 *
 * @todo Use a plugin manager to get global settings from Component plugins.
 */
trait AFrameFormatterTrait {

  /**
   * Get the global default settings.
   *
   * @return array
   *   The default global settings.
   */
  public static function globalDefaultSettings() {
    $defaults['aframe_components'] = [];

    return $defaults;
  }

  /**
   * The form for the global settings form.
   *
   * @param array $form
   *   The form array.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The details form element for the components.
   */
  protected function globalSettingsForm(array $form, FormStateInterface $form_state) {
    $element['aframe_components'] = [
      '#type'   => 'details',
      '#title'  => t('Additional components'),
    ];

    /** @var \Drupal\aframe\AFrameComponentPluginManager $component_manager */
    $component_manager = $this->componentManager;
    $components = $component_manager->getDefinitions();
    foreach ($components as $component) {
      if (isset($this->getSetting('aframe_components')[$component['id']])) {
        /** @var \Drupal\aframe\AFrameComponentPluginInterface $component_instance */
        $component_instance = $component_manager->createInstance($component['id'], [
          'settings' => [
            $component['id'] => $this->getSetting('aframe_components')[$component['id']],
          ],
        ]);
        $element['aframe_components'][$component['id']] = $component_instance->settingsForm($form, $form_state);
      }
    }

    return $element;
  }

  /**
   * Ajax callback for components.
   *
   * @param array $form
   *   The form array.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed
   *   The return value of the callback.
   */
  public function callbackComponentAjax(array $form, FormStateInterface $form_state) {
    $parents = $form_state->getTriggeringElement()['#array_parents'];
    array_pop($parents);
    return NestedArray::getValue($form, $parents);
  }

  /**
   * Get the global settings summary.
   *
   * @return array
   *   The global settings summary.
   */
  protected function globalSettingsSummary() {
    // @fixme Feature to investigate.
    // $summary = [];
    /** @var \Drupal\aframe\AFrameComponentPluginManager $component_manager */
    $component_manager = $this->componentManager;
    $components = $component_manager->getDefinitions();
    foreach ($components as $component) {
      if (isset($this->getSetting('aframe_components')[$component['id']])) {
        /** @var \Drupal\aframe\AFrameComponentPluginInterface $component_instance */
        $component_instance = $component_manager->createInstance($component['id'], [
          'settings' => [
            $component['id'] => $this->getSetting('aframe_components')[$component['id']],
          ],
        ]);
        if ($component_instance->settingsSummary()) {
          // @fixme Feature to investigate.
          // $summary[] = $component_instance->settingsSummary();
        }
      }
    }
    // @fixme Feature to investigate.
    // return $summary;
    return [];
  }

  /**
   * Function to get the attributes.
   *
   * @return array
   *   An array with the attributes.
   */
  protected function getAttributes() {
    $attributes = [];

    /** @var \Drupal\aframe\AFrameComponentPluginManager $component_manager */
    $component_manager = $this->componentManager;
    $components = $component_manager->getDefinitions();
    foreach ($components as $component) {
      if (isset($this->getSetting('aframe_components')[$component['id']])) {
        /** @var \Drupal\aframe\AFrameComponentPluginInterface $component_instance */
        $component_instance = $component_manager->createInstance($component['id'], [
          'settings' => [
            $component['id'] => $this->getSetting('aframe_components')[$component['id']],
          ],
        ]);
        $value = $component_instance->getValue();
        if ($value) {
          $attributes[$component['id']] = $value;
        }
      }
    }

    return $attributes;
  }

}
