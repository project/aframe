<?php

namespace Drupal\aframe\Services;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Class AFrameLibraryDiscovery.
 *
 * @package Drupal\aframe
 */
class AFrameLibraryDiscovery {

  /**
   * Path to aframe library.
   *
   * @var string
   */
  protected $aframeLibraryPath;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs a new AFrameLibraryDiscovery object.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   */
  public function __construct(LoggerChannelFactoryInterface $logger_factory) {
    $this->logger = $logger_factory->get('aframe');

    $this->aframeLibraryPath = DRUPAL_ROOT . base_path() . 'libraries/aframe/dist/';
  }

  /**
   * Function to get the different versions of the aframe library.
   */
  public function aframeScanLibraryVersions() {
    $aframe_versions = [];
    $handle = @opendir($this->aframeLibraryPath) or $this->logger->notice("Unable to open " . $this->aframeLibraryPath);
    while ($entry = @readdir($handle)) {
      $matches = [];
      if (is_file($this->aframeLibraryPath . $entry) && preg_match('/^aframe-v(.*)\.min\.js$/i', $entry, $matches)) {
        $aframe_versions[$matches[1]] = base_path() . 'libraries/aframe/dist/' . $entry;
      }
    }
    if ($handle) {
      closedir($handle);
    }

    // Sort by version.
    krsort($aframe_versions);

    return $aframe_versions;
  }

}
